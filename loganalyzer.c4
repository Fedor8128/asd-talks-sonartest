workspace "LogAnalyzer" "This is the model for the LogAnalyzer workshop." {

    model {
        customer = person "Customer" "Someone that uses a client application" "System User"
        operator = person "Operator" "Someone that uses the log analyzer" "System User"

        logAnalyzer = softwareSystem "LogAnalyzer" "Application that logs but also is able to analyze these logs." {
            tomEE = container "LogService" "Enables REST-ful services to the logger" "Java EE" "Tomcat" {
                fileLogService = component "LogService" "Offers logging feature" "Java Module/Component"

            }
            logAnalyzerProcess = container "Java Main Process" "Host Process for the LogAnalyzer" "Java" "Java" {
                simpleLogAnalyzer = component "LogAnalyzer" "Analyzes logs" "Java" "Use Case"
                mailService = component "MailService" "Sends mail in case of analysis problems" "Java" "Use Case"
                guice = component "Guice" "Dependency Injection Library" "Java" "Library"
            }
        }

        clientApplication = softwareSystem "Client Application" "Application that uses the LogAnalyzer." {
            clientProcess = container "Java Main Process" "Host Process for the client" "Java" "Java" {
                clientComponent = component "Client Process" "Actual calling the logger" "Java" "Use Case"
            }

        }
        gmail = softwareSystem "GMail" "Mail service from Google." "External" {
        }

        customer -> clientProcess "Uses"
        operator -> logAnalyzerProcess "Uses"
        clientApplication -> fileLogService "JSON/HTTPs"
        simpleLogAnalyzer -> fileLogService "JSON/HTTPs"
        simpleLogAnalyzer -> guice "Uses"
        simpleLogAnalyzer -> mailService "Uses service to notify operators"
        mailService -> gmail "SMTP"
    }

    views {
        systemContext logAnalyzer "SystemContext" "An example of a System Context diagram." {
            include *
            autoLayout
        }

        container logAnalyzer "LogAnalyzer-Container" "Container View" {
            include *
            autoLayout
        }

        container clientApplication "ClientApplication-Container" "Container View" {
            include *
            autoLayout
        }

        component logAnalyzerProcess "LogAnalyzer-Components" "" {
            include *
            autoLayout
        }

        component tomEE "TomEE-Components" "" {
           include *
           autoLayout
        }

        styles {
            element "Software System" {
                background #1168bd
                color #ffffff
            }
            element "System User" {
                shape person
                background #08427b
                color #ffffff
            }
            element "External" {
                background #808080
                color #ffffff
            }
            element "Java" {
                shape hexagon
                background #1180bf
                color #ffffff
            }
            element "Tomcat" {
                background #1180ba
                color #ffffff
            }
            element "Use Case" {
                shape hexagon
                background #1180ba
                color #ffffff
            }
            element "Library" {
                shape component
                background #1180ba
                color #ffffff
            }
        }
    }
}
